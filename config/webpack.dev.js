const webpackMerge = require('webpack-merge')
const OpenBrowserPlugin = require('open-browser-webpack-plugin')
const webpack = require('webpack')

const helpers = require('./helpers')
const commonConfig = require('./webpack.common')

// const apiBaseHost = 'localhost'

module.exports = webpackMerge(commonConfig, {
  progress: false,
  devServer: {
    outputPath: helpers.root('dist'),
    historyApiFallback: true,
    compress: true,
    port: helpers.config.webserver.basePort,
    hot: true,
    inline: true
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new OpenBrowserPlugin({
        url: `http://localhost:${helpers.config.webserver.basePort}`
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
});
