const webpack = require('webpack');
const glob = require('glob-all');
const helpers = require('./helpers');
const path = require('path');

// Webpack plugins
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // The base directory (absolute path!) for resolving the entry option.
  context: helpers.root('src'),
  // -----> Entry
  entry: {
    polyfills: './polyfills.js',
    main: './app/index.js',
    styles: glob.sync([
      // '../node_modules/anything there …
    ], { cwd: helpers.root('src') }),
  },
  // Output ----->
  output: {
    path: helpers.root('dist'),
    filename: helpers.getOutputFile('js'),
    publicPath: '/'
  },
  //
  resolve: {
    // An array of extensions that should be used to resolve modules.
    extensions: ['', '.js', '.ico', '.scss'],
    // The directory (absolute path) that contains your modules.
    root: helpers.root('src'),
    // An array of directory names to be resolved to the current directory as well as its ancestors, and searched for modules.
    modulesDirectories: ['src', '', 'node_modules'],
  },
  module: {
    // How webpack should process specific extensions
    loaders: [
      { test: /\.js$/, loaders: ['ng-annotate', 'babel?presets[]=es2015'] },
      { test: /\.html$/, loader: 'html' },
      { test: /\.scss$/, loaders: ['style', 'css', 'sass'] },
      { test: /\.json$/, loader: 'json' },
      { test: /\.less$/, loader: ExtractTextPlugin.extract(['css', 'less']) },
      { test: /\.css/, loader: ExtractTextPlugin.extract(['css']) }
    ],
  },
  // Additional plugins
  plugins: [
    // Cleaning directories
    new CleanWebpackPlugin(['dist', 'prod'], { verbose: true, root: helpers.root() }),
    // Bundling CSS files instead of inlining in js
    // new ExtractTextPlugin(helpers.getOutputFile('css')),
    // Inject, generate HTML
    new HtmlWebpackPlugin({
      title: 'PGS ES6',
      xhtml: true,
      template: 'index.ejs',
      favicon: 'assets/favicon.ico',
      chunksSortMode: helpers.packageSort(['polyfills', 'main', 'styles'])
    }),
    // Copy static assets
    new CopyWebpackPlugin([
      { from:'{fonts,img,svg,mocks}/**/*', to: 'Assets', context: 'rag-interface'},
    ], {}),
  ],
}
