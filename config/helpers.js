const path = require('path')

const _root = path.resolve(__dirname, '..')

console.log('root directory:', root())

const getOutputFile = ext => 'assets/' + ext + '/[name]-[hash].' + ext

function root() {
  return path.join.apply(path, [_root].concat(Array.prototype.slice.call(arguments)));
}

const packageSort = packages => (a, b) => packages.indexOf(a.names[0]) - packages.indexOf(b.names[0])

exports.root = root
exports.packageSort = packageSort
exports.getOutputFile = getOutputFile

var config = {}
try {
  config = require('../.dev-config.json')
} catch (e) {
  console.log('Using defaul config')
}
exports.config = Object.assign(require('./config.json'), config)
