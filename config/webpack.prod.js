const webpack = require('webpack');
const webpackMerge = require('webpack-merge')

const helpers = require('./helpers')
const commonConfig = require('./webpack.common')

const DefinePlugin = webpack.DefinePlugin;
const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');

module.exports = webpackMerge(commonConfig, {
    output: {
        path: helpers.root('prod'),
    },
    loaders: [
        { test: /\.scss$/, loaders: ExtractTextPlugin.extract(['css', 'scss']) }
    ],
    plugins: [
        new DefinePlugin({
          'process.env': {
            'NODE_ENV': JSON.stringify('production')
          }
        }),
        new UglifyJsPlugin({
          compress:{
            warnings: true,
            unsafe: true,
            drop_console: true,
            keep_fargs: false
          },
          comments: false
        }),
        FailPlugin
  ]

});
