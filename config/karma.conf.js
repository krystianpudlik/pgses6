module.exports = function (config) {
  const helpers = require('./helpers');
  const webpackConfig = require('./webpack.test')

  config.set({
    basePath: helpers.root('src'),
    frameworks: ['jasmine'],
    files: [
      'spec.js',
      '**/*.spec.js'
    ],
    exclude: [],
    preprocessors: { '**/*.js': ['webpack', 'sourcemap'] },
    webpack: webpackConfig,
    webpackServer: { noInfo: true },
    reporters: ['mocha'],
    port: 9876,
    colors: false,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true,
    concurrency: 6e6
  })
}