// const webpack = require('webpack');
const webpackMerge = require('webpack-merge')

// const helpers = require('./helpers')
const commonConfig = require('./webpack.common')

module.exports = webpackMerge(commonConfig, {
  devtool: 'inline-source-map'
})
