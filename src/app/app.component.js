class AppController {
    constructor(sampleService) {
        'ngInject';

        this.sampleService = sampleService;
    }

    $onInit() {
        this.sampleService.getPersonAsync()
            .then((person) => this.person = person);
    }
}

const appComponent = {
    bindings: {},
    template: require('./app.component.html'),
    controller: AppController
};

export default appComponent;