// Vendor libraries
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngMaterial from 'angular-material';
import appComponent from './app.component';
import componentsModule from './components';
import samplesModule from './samples';
// Vendor styles
import 'angular-material/angular-material.scss';


const appModule = angular
    .module('pgsapp', [ngMaterial, uiRouter, samplesModule, componentsModule]);

appModule
    .config(($stateProvider, $urlRouterProvider) => {
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('app', {
                url: '/',
                template: '<app-component></app-component>'
            });
    })
    .component('appComponent', appComponent);;
