import './footer.component.scss';

const footerComponent = {
    template: require('./footer.component.html')
};

export default footerComponent;