import angular from 'angular';
import footerComponent from './footer.component';

const footerModule = angular.module('pgsapp.footer', []);

footerModule
    .component('footerComponent', footerComponent);

export default footerModule.name;