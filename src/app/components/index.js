import angular from 'angular';
import footerModule from './footer';
import headerModule from './header';

const componentsModule = angular
    .module('pgsapp.components', [footerModule, headerModule])

export default componentsModule.name;