import './header.component.scss';

const headerComponent = {
    template: require('./header.component.html'),
    bindings: {
        name: '@'
    }
};

export default headerComponent;