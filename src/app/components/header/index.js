import angular from 'angular';
import headerComponent from './header.component';

const headerModule = angular.module('pgsapp.header', []);

headerModule
    .component('headerComponent', headerComponent);

export default headerModule.name;