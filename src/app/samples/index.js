import angular from 'angular';
import SampleService from './sample.service';
import SampleClassDirective from './sample-class.directive';

const samplesModule = angular
    .module('pgsapp.samples', [])
    .service('sampleService', SampleService)
    .directive('sampleClassDirective', SampleClassDirective);

export default samplesModule.name;