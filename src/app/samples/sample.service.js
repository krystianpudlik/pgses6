class SampleService {
    constructor($http) {
        'ngInject';

        this.$http = $http;
    }

    getPersonAsync() {
        return this.$http.get('http://beta.json-generator.com/api/json/get/V1yvZiMdb')
            .then((response) => response.data);
    }
}

export default SampleService;