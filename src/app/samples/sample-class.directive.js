class SampleClassDirective {
    constructor() {
        this.restrict = 'A';
        this.controller = SampleClassDirectiveController;
    }

    link($scope, $element, $attrs, $ctrl) {
        $ctrl.$log.info('Info!');
    }

    static factory() {
        return new SampleClassDirective();
    }
}

class SampleClassDirectiveController {
    constructor($log) {
        'ngInject';

        this.$log = $log;
    }
}

export default SampleClassDirective.factory;